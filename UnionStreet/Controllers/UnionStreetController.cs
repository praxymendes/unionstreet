﻿using BuisnessObjects.UnionStreet;
using Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UnionStreet.Controllers
{
    //README.TXT
    //To Sortby - click on header index
    //Access buinsess logic project to get, post, put data on server.
    //DataAccessLayer project - to access database; used entity framework
    //BuisnessLogic - For additional filtering, logic - once data is returned from data access layer
    //BuisnessObjects - For models
    public class UnionStreetController : Controller
    {
        private UnionStreetBuisnessLogic BL;
        public UnionStreetController()
        {
            BL = new UnionStreetBuisnessLogic();
        }
        public ActionResult Index(string SortType, string currentSort)
        {
            if (currentSort == null || currentSort == "desc")
                currentSort = "asc";
            else
                currentSort = "desc";
            
            var lstDetails = BL.GetDetails(0, SortType, currentSort);
            return View(lstDetails);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Details model)
        {
            BL.Save(model);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? detailId)
        {
            if (!detailId.HasValue)
                return HttpNotFound();
            var lstDetails = BL.GetDetails(detailId.Value, "Company", "asc");
            return View("Edit",lstDetails[0]);
        }

        [HttpPost]
        public ActionResult Edit(Details model)
        {
            BL.Save(model);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int detailId)
        {
            BL.Delete(detailId);
           return RedirectToAction("Index");
        }
    }
}
