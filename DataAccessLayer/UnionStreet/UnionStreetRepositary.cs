﻿using BuisnessObjects.UnionStreet;
using DataAccessLayer.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.UnionStreet
{
    public class UnionStreetRepositary
    {
        private UnionStreetEntities db;
        public UnionStreetRepositary()
        {
            if (db == null)
                db = new UnionStreetEntities();
        }
        public List<Details> GetDetails()
        {
            var details = (from d in db.Details
                          join c in db.Companies on d.CompanyId equals c.Id
                          join s in db.Sites on d.SiteId equals s.Id
                          select new Details
                          {
                              Company = new BuisnessObjects.UnionStreet.Company
                              {CompanyId = d.CompanyId.Value, CompanyName=c.CompanyName},
                              Site = new BuisnessObjects.UnionStreet.Site
                              {Id = s.Id,SiteName=s.SiteName},
                              MainSite = d.MainSite.HasValue ? d.MainSite.Value : false,
                              Notes = d.Notes,
                              PhoneNumber = d.PhoneNumber,
                              StartDate = d.StartDate.HasValue ? d.StartDate.Value : DateTime.MinValue,
                              EndDate = d.EndDate.HasValue ? d.EndDate.Value : DateTime.MinValue,
                              DetailsId = d.Id
                          }).ToList();


            return details;
        }

        public void DeleteDetail(int detailId)
        {
            var deleteObj = db.Details.SingleOrDefault(x => x.Id == detailId);
            if (deleteObj != null) {
                db.Details.Remove(deleteObj);
                db.SaveChanges();
            }

        }

        public void SaveDetail(Details model)
        {
            var deleteObj = db.Details.SingleOrDefault(x => x.Id == model.DetailsId);
            if (deleteObj != null)
            {
                //Add details
                var updatedObj = new EntityFramework.Detail
                {
                    CompanyId = model.Company.CompanyId,
                    SiteId = model.Site.Id,
                    MainSite = model.MainSite,
                    Notes = model.Notes,
                    PhoneNumber = model.PhoneNumber,
                    StartDate = model.StartDate,
                    EndDate = model.EndDate,
                    Id = model.DetailsId
                };
                ((IObjectContextAdapter)db).ObjectContext.Detach(deleteObj);
                db.Entry(updatedObj).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                var company = db.Companies.Where(x => x.CompanyName == model.Company.CompanyName).SingleOrDefault();
                var site = db.Sites.Where(x => x.SiteName == model.Site.SiteName).SingleOrDefault();
                
                //Insert into company
                if (company == null)
                {
                    company = new EntityFramework.Company{CompanyName= model.Company.CompanyName};
                    model.Company.CompanyId = db.Companies.Add(company).Id;
                    db.SaveChanges();
                }

                //Insert into Site
                if (site == null)
                {
                    site = new EntityFramework.Site{SiteName= model.Site.SiteName};
                    model.Site.Id = db.Sites.Add(site).Id;
                    db.SaveChanges();
                }

                //Add details
                var detailsModel = new EntityFramework.Detail{
                    CompanyId = company.Id,
                    SiteId = site.Id,
                    MainSite = model.MainSite,
                    Notes = model.Notes,
                    PhoneNumber = model.PhoneNumber,
                    StartDate = model.StartDate,
                    EndDate = model.EndDate
                };

                db.Details.Add(detailsModel);
                db.SaveChanges();
            }
            }

        }
}
