﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Implementation;
using BuisnessObjects.UnionStreet;

namespace UnionStreet.Tests
{
    [TestClass]
    public class UnionStreetControllerTest
    {
        private UnionStreetBuisnessLogic BL;
        public UnionStreetControllerTest()
        {
            BL = new UnionStreetBuisnessLogic();
        }
        [TestMethod]
        public void Can_Get_details()
        {
            var lstDetails = BL.GetDetails(0, "Company", "asc");
            Assert.IsTrue(lstDetails.Count > 0, "Passed");
        }

        [TestMethod]
        public void Can_Create_New()
        {
            
            BL.Save(GetModel());
            var lstDetails = BL.GetDetails(0, "Company", "asc").Find(x=>x.Company.CompanyName=="testCompany");
            Assert.IsTrue(lstDetails != null, "Passed");
        }

        private Details GetModel (){
            return new Details
            {
                Company = new Company { CompanyName = "testCompany" },
                Site = new Site { SiteName = "testSite" },
                MainSite = true,
                Notes = "testNote",
                PhoneNumber = "123123",
                StartDate = DateTime.MinValue,
                EndDate = DateTime.MinValue
            };
        }
    }
}
