﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DataAccessLayer.Test
{
    [TestClass]
    public class UnionStreetRepositaryTest
    {
        private DataAccessLayer.UnionStreet.UnionStreetRepositary _repositary;
        public UnionStreetRepositaryTest()
        {
            _repositary = new DataAccessLayer.UnionStreet.UnionStreetRepositary();
        }
        [TestMethod]
        public void Can_Get_Details()
        {
            var details = _repositary.GetDetails();

            Assert.IsTrue(details.Count > 0);
        }
    }
}
