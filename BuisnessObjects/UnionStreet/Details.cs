﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BuisnessObjects.UnionStreet
{   //To do: more validation
    public class Details
    {
        public int DetailsId { get; set; }
        public Company Company { get; set; }
        public Site Site { get; set; }
        public bool MainSite { get; set; }
        public string Notes { get; set; }
        public string PhoneNumber { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDate { get; set; }
        public string CurrentSort{ get; set; }
    }
}
