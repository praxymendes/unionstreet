﻿using BuisnessObjects.UnionStreet;
using DataAccessLayer.UnionStreet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementation
{
    public class UnionStreetBuisnessLogic
    {
        private UnionStreetRepositary _repositary;
        public UnionStreetBuisnessLogic()
        {
            _repositary = new UnionStreetRepositary();
        }
          
        public List<BuisnessObjects.UnionStreet.Details> GetDetails(int detailId, string sortBy, string currentSort= "asc")
        {
            var obj = new DataAccessLayer.UnionStreet.UnionStreetRepositary();
            var detail = obj.GetDetails();
            detail = detailId > 0 ? detail.Where(x => x.DetailsId == detailId).ToList() : detail;

            foreach (var d in detail)
            {
                d.CurrentSort = currentSort;
            }

            //Sort
            switch (sortBy)
            {
                case "Company":
                    detail = currentSort == "asc" ? detail.OrderBy(p => p.Company.CompanyName).ToList() : detail.OrderByDescending(p => p.Company.CompanyName).ToList();
                    break;
                case "Site":
                    detail = currentSort == "asc" ? detail.OrderBy(p => p.Site.SiteName).ToList()
                        : detail.OrderByDescending(p => p.Site.SiteName).ToList();
                    break;
                case "MainSite":
                    detail = currentSort == "asc" ? detail.OrderBy(p => p.MainSite).ToList()
                        : detail.OrderByDescending(p => p.MainSite).ToList();
                    break;
                case "Notes":
                    detail = currentSort == "asc" ? detail.OrderBy(p => p.Notes).ToList()
                        : detail.OrderByDescending(p => p.Notes).ToList();
                    break;
                case "PhoneNumber":
                    detail = currentSort == "asc" ? detail.OrderBy(p => p.PhoneNumber).ToList()
                        : detail.OrderByDescending(p => p.PhoneNumber).ToList();
                    break;
                case "StartDate":
                    detail = currentSort == "asc" ? detail.OrderBy(p => p.StartDate).ToList()
                       : detail.OrderByDescending(p => p.StartDate).ToList();
                    break;
                case "EndDate":
                    detail = currentSort == "asc" ? detail.OrderBy(p => p.EndDate).ToList()
                       : detail.OrderByDescending(p => p.EndDate).ToList();
                    break;
            }

            return detail;
        }

        public void Delete(int detailId)
        {
            _repositary.DeleteDetail(detailId);
        }

        public void Save(Details model)
        {
            _repositary.SaveDetail(model);
        }
    }
}
